  
SHELL:=/bin/bash

.PHONY: test

dep: ## install python dependencies
	pipenv install

dvc_config: ## init dvc and add the data storage
	dvc init


dvc_add: ## Add the data dir to dvc.
	dvc add data/
	dvc push
